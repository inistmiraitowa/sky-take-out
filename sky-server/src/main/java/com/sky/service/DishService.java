package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {

    /*
    新增菜品和相关口味
     */
    void saveWithFlavor(DishDTO dishDTO);

    /*
    菜品的分页查询
     */
    PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO);

    /*
    删除菜品
     */
    void deleteBatch(List<Long> ids);
/*
根据id查询菜品和对应的口味数据
 */

    DishVO getByIdWithFlavor(Long id);
/*
根据id来修改菜品的基本信息和口味的信息
 */
    void updateWithFlaor(DishDTO dishDTO);
/*
菜品起售停售
 */
    void startOrStop(Integer status, Long id);
}
